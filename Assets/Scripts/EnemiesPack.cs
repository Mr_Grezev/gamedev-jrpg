﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesPack {

    Dictionary<string, Enemy[]> locationEnemies = new Dictionary<string, Enemy[]>();

    public EnemiesPack()
    {
        Enemy[] array = new Enemy[4];
        array[0] = new Racoon();
        array[1] = new Raven();
        array[2] = new HellDog();
        array[3] = new MagmaBear();
        locationEnemies.Add("MobPack1", array);
    }

    public Enemy[] GetPackByLocation(string pack)
    {
        Enemy[] enemies = new Enemy[5];
        Enemy[] allEnemies = locationEnemies[pack];
        for (int i = 0; i < 5; i++)
        {
            enemies[i] = allEnemies[Random.Range(0, allEnemies.Length)].Clone();
            Debug.Log(enemies[i].forename);
        }
        return enemies;
    }

    
}
