﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;


public class SaveAndLoad : MonoBehaviour {

    [System.Serializable]
    public class SavedData
    {
        public Player player;
        public float x;
        public float y;
        public string location;
        public bool fromMenu;
        public int session;
        public Dictionary<string, bool> points;
    }

    public void Save(GameObject player, PlayerData playerData, bool fromMenu)
    {
        SavedData position = new SavedData();
        position.x = player.transform.position.x;
        position.y = player.transform.position.y;
        position.location = playerData.location;
        position.fromMenu = fromMenu;
        position.player = playerData.player;
        position.points = playerData.points;
        position.session = playerData.session;

        if (!Directory.Exists(Application.dataPath + "/Saves"))
            Directory.CreateDirectory(Application.dataPath + "/Saves");
        FileStream fs = new FileStream(Application.dataPath + "/Saves/Save.sv", FileMode.Create);
        BinaryFormatter formator = new BinaryFormatter();
        formator.Serialize(fs, position);
        fs.Close();
    }

    public bool Load(GameObject player, PlayerData playerData)
    {
        if (File.Exists(Application.dataPath + "/Saves/Save.sv"))
        {
            FileStream fs = new FileStream(Application.dataPath + "/Saves/Save.sv", FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                SavedData pos = (SavedData)formatter.Deserialize(fs);
                if (!pos.fromMenu || pos.session == playerData.session)
                {
                    fs.Close();
                    return false;
                }
                player.transform.position = new Vector3(pos.x, pos.y);
                playerData.player = pos.player;
                playerData.points = pos.points;
                fs.Close();
                Save(new GameObject(), new PlayerData(), false);
                return pos.fromMenu;
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
                SceneManager.LoadScene("Menu");
                fs.Close();
                return false;
            }
        }
        else
        {
            Application.Quit();
            return false;
        }
    }

    public string LoadScene()
    {
        if (File.Exists(Application.dataPath + "/Saves/Save.sv"))
        {
            FileStream fs = new FileStream(Application.dataPath + "/Saves/Save.sv", FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                string scene;
                SavedData pos = (SavedData)formatter.Deserialize(fs);
                scene = pos.location;
                fs.Close();
                return scene;
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
                SceneManager.LoadScene("Menu");
                fs.Close();
                return "";
            }
        }
        else
        {
            Application.Quit();
            return "";
        }
    }
}
