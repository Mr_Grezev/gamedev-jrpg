﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public abstract class Magic {

    public float time = 0;
    public int damage, type;
    public string forename;

    public static class Type
    {
        public const int NONE = -1;
        public const int NORMAL = 0;
        public const int FIRE = 1;
        public const int WATER = 2;
        public const int WIND = 3;
        public const int GROUND = 4;
        public const int LIGHT = 5;
        public const int DARKNESS = 6;
    }

    public abstract GameObject PlayAnimaion(GameObject person);
    public abstract IEnumerator timerDestroy(GameObject destroy, float time);
}
