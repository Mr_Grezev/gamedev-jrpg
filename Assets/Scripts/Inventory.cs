﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Inventory : MonoBehaviour {

    bool isShow = false;
    int selectedItem = 0;
    int top, bottom;
    int last;
    Player player;

    int MenuSelection(string direction)
    {
        if (direction == "up")
        {
            if (selectedItem == 0)
            {
                selectedItem = last;
                bottom = last;
                if (last > 1) top = last - 6;
                else top = 0;
            }
            else
            {
                selectedItem -= 1;
                if (selectedItem == top - 1)
                {
                    top--;
                    bottom--;
                }
            }
        }

        if (direction == "down")
        {
            if (selectedItem == last)
            {
                selectedItem = 0;
                top = 0;
                if (last < 2) bottom = last;
                else bottom = 6;
            }
            else
            {
                selectedItem += 1;
                if (selectedItem == bottom + 1)
                {
                    top++;
                    bottom++;
                }
            }
        }

        return selectedItem;
    }
	
    void OnGUI()
    {
        if (isShow)
        {
            GUI.Box(new Rect((Screen.width / 2) - 100, (Screen.height / 2) - 150, 200, 150), "Мини меню");
            for (int i = top; i <= bottom && player.items[i] != null; i++)
            {
                int g = i - top;
                GUI.SetNextControlName(player.items[i].name + i.ToString());
                GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 100 + (g * 20), 150, 25), player.items[i].name);
            }
            GUI.FocusControl(player.items[selectedItem].name + selectedItem.ToString());
        }
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            selectedItem = MenuSelection("down");
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            selectedItem = MenuSelection("up");
        }
        if (Input.GetKeyDown(KeyCode.I) && SceneManager.GetActiveScene().name != "Fight")
        {
            isShow = !isShow;
            Debug.Log("nya");
            player = GameObject.Find("PlayerData").GetComponent<PlayerData>().player;
            System.Array.Sort(player.items);
            System.Array.Reverse(player.items);
            for (int i = 0; i < player.items.Length; i++)
            {
                if (player.items == null)
                {
                    last = i - 1;
                    break;
                }
            }
        }
	}
}
