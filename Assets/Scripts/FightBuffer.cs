﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FightBuffer {
    
    public void Fight(bool isBoss, string pack)
    {
        Transform player = GameObject.Find("Main Camera").GetComponent<Transform>();
        PlayerData playerData = GameObject.Find("PlayerData").GetComponent<PlayerData>();
        playerData.isBoss = true;
        playerData.pack = pack;
        playerData.x = player.position.x;
        playerData.y = player.position.y;
        SceneManager.LoadScene("Fight");
    }

    public const string MOB_PACK_1 = "MobPack1";

}
