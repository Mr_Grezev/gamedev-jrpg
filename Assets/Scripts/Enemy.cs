﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Being, IComparable<Enemy> {
    
    public int damage, weakness, speed;
    public float actionTime = 3f;
    public float defaultActionTime = 3f;
    public GameObject gameObject;

    public void SetDefaults()
    {
        forename = prefab;
    }

    public void StartAnimation()
    {
        if (gameObject == null)
        {
            Debug.Log("Game Object is empty.");
            return;
        }
    }

    public int GetRealDamage()
    {
        return damage + (lv * 2);
    }

    public int CompareTo(Enemy other)
    {
        return prefab.CompareTo(other.prefab);
        throw new NotImplementedException();
    }

    public Enemy Clone()
    {
        Enemy enemy = new Enemy();
        enemy.hp = hp;
        enemy.mp = mp;
        enemy.lv = lv;
        enemy.damage = damage;
        enemy.prefab = prefab;
        enemy.speed = speed;
        enemy.weakness = weakness;
        enemy.forename = forename;
        return enemy;
    }
}
