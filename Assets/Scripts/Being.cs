﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Being {
    public int hp, mp, lv;
    public int currentHealth;
    public string forename, prefab;
}
