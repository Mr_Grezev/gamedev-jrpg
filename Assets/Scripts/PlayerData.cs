﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {

	public Player player;
    public string location;
    public string pack;
    public Dictionary<string, bool> points = new Dictionary<string, bool>();
    public bool isBoss;
    public float x, y;
    public bool fightLocation;
    public int session;
}
