﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Warp : MonoBehaviour {

    public string to;

    void OnTriggerEnter2D (Collider2D col)
    {

        SceneManager.LoadScene(to);
    }
}
