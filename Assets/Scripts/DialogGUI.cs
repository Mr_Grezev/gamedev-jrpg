﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogGUI : MonoBehaviour {
    
    float time;
    Queue<string> messeages = new Queue<string>();

    void OnGUI()
    {
        if (messeages.Count != 0)
        {
            GUIStyle dialogStyle = new GUIStyle(GUI.skin.box);
            dialogStyle.alignment = TextAnchor.UpperLeft;
            dialogStyle.font = Font.CreateDynamicFontFromOSFont("Comic Sans MS", 16);
            dialogStyle.richText = true;
            GUI.Box(new Rect(Screen.width/16, Screen.height / 32, Screen.width - (Screen.width / 8), 100), messeages.Peek());
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (messeages.Count != 0) messeages.Dequeue();
            if (messeages.Count == 0)
            {
                EndDialog();
            }
        }
    }

    void StartDialog()
    {
        time = Time.timeScale;
        Time.timeScale = 0;
    } 

    void EndDialog()
    {
        Time.timeScale = time;
    }

    public void AddDialogMesseage(string speaker, string text)
    {
        if (messeages.Count == 0)
        {
            StartDialog();
        }
        messeages.Enqueue(speaker + ":\n" + text);
    }

}
