﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Person : Being, IComparable<Person> {

    public int exp, speed;
    public float actionTime = 3f;
    public float defaultActionTime = 3f;
    public Weapon weapon;
    public Magic[] magic = new Magic[1000];
    [NonSerialized]
    public GameObject gameObject;
    public bool isDead = false;
    
    public Person(string name, string prefab)
    {
        this.prefab = prefab;
        forename = name;
        hp = 30;
        mp = 10;
        exp = 0;
        lv = 1;
        speed = 5;
        weapon = new Knuckles();
        currentHealth = hp;
    }

    public Person()
    {

    }

    public void SetParams(string name, string prefab, Weapon weapon,  int hp = 10, int mp = 10, int lv = 1, int speed = 5)
    {
        this.hp = hp;
        this.mp = mp;
        this.lv = lv;
        this.prefab = prefab;
        this.speed = speed;
        if (weapon != null)
        {
            this.weapon = weapon;
        }
        else this.weapon = new Knuckles();
        currentHealth = hp;
        forename = name;
        exp = 0;
    }
    
    public void LevelUp(int hp = 10, int mp = 10, int speed = 5)
    {
        this.hp += hp;
        this.mp += mp;
        this.speed += speed;
        actionTime -= speed / 1000;
        lv += 1;
    }

    public int CompareTo(Person y)
    {
        return forename.CompareTo(y.forename);
        throw new System.NotImplementedException();
    }
}
