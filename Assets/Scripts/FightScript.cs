﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FightScript : MonoBehaviour {
    string[] menuItems = new string[4];
    int selectedItem = 0;
    Player player;
    Enemy[] enemies;
    int realSize = 0;
    int cast = 0;
    Person movingPerson = null;
    bool isReady = false;
    bool isAttack = false;
    bool isMagic = false;
    bool isItem = false;
    bool isPerson = false;
    bool stop = false;
    int top = 0, bottom = 2;
    float x, y;

    void Start()
    {
        //// Заглушка
        //Player player = new Player();
        ////player.items[0] = new HealthPotion();
        //player.persons[0] = new Alex();
        //player.persons[0].magic[0] = new FireBall();
        //player.persons[1] = new Elena();
        //player.persons[2] = new Den();
        //player.persons[3] = new Person("Lya", "Wasp");
        //Enemy[] enemies = new Enemy[5];
        //enemies[0] = new HellDog();
        //enemies[1] = new Raven();
        //enemies[2] = new Racoon();
        //enemies[3] = new Raven();
        //enemies[4] = new Raven();
        //// // Конец заглушки
        //this.player = player;
        //this.enemies = enemies;

        Destroy(GameObject.Find("DialogBulider"));
        GameObject playerData = GameObject.Find("PlayerData");
        player = playerData.GetComponent<PlayerData>().player;
        EnemiesPack pack = new EnemiesPack();
        enemies = pack.GetPackByLocation(playerData.GetComponent<PlayerData>().pack);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        ShowPlayerPersons(player);
        ShowEnemies(enemies);
        LoadGUI();
    }

    void Update()
    {
        KeyboardControl();
        BattleTiming();
    }

    void OnGUI()
    {
        if (isReady)
        {
            GUI.Box(new Rect((Screen.width / 2) - 30, (Screen.height / 2) + 30, 200, 125), "Select an action:");
            if (movingPerson != null)
            {
                GUI.Label(new Rect(Screen.width / 2 + 55, Screen.height / 2 + 50, 100, 20), movingPerson.forename);
            }
            for (int i = top; i <= bottom; i++)
            {
                int g = i - top;
                GUI.SetNextControlName(menuItems[i]);
                GUI.Button(new Rect(Screen.width / 2 + 20,  Screen.height / 2 + 80 + (20 * g) + (3 * g), 100, 20), menuItems[i]);
            }
            GUI.FocusControl(menuItems[selectedItem]);
        }
        if (isAttack)
        {
            GUI.Box(new Rect((Screen.width / 2) - 30, (Screen.height / 2) + 30, 200, 125), "Select an action");
            if (movingPerson != null)
                GUI.Label(new Rect(Screen.width / 2 + 55, Screen.height / 2 + 50, 100, 20), "Attack");
            for (int i = top; i <= bottom && enemies[i] != null; i++)
            {
                int g = i - top;
                GUI.SetNextControlName(enemies[i].prefab + i.ToString());
                GUI.Button(new Rect(Screen.width / 2 + 20, Screen.height / 2 + 80 + (20 * g) + (3 * g), 100, 20), enemies[i].prefab);
            }
            GUI.FocusControl(enemies[selectedItem].prefab + selectedItem.ToString());
        }
        if (isMagic && !isAttack)
        {
            GUI.Box(new Rect((Screen.width / 2) - 30, (Screen.height / 2) + 30, 200, 125), "Select an action");
            if (movingPerson != null)
                GUI.Label(new Rect(Screen.width / 2 + 55, Screen.height / 2 + 50, 100, 20), "Magic");
            for (int i = top; i <= bottom && movingPerson.magic[i] != null; i++)
            {
                int g = i - top;
                GUI.SetNextControlName(movingPerson.magic[i].forename);
                GUI.Button(new Rect(Screen.width / 2 + 20, Screen.height / 2 + 80 + (20 * g) + (3 * g), 100, 20), movingPerson.magic[i].forename);
            }
            GUI.FocusControl(movingPerson.magic[selectedItem].forename);
        }
        if (isItem && !isAttack && !isPerson)
        {
            GUI.Box(new Rect((Screen.width / 2) - 30, (Screen.height / 2) + 30, 200, 125), "Select an action");
            if (movingPerson != null)
                GUI.Label(new Rect(Screen.width / 2 + 55, Screen.height / 2 + 50, 100, 20), "Items");
            for (int i = top; i <= bottom && player.items[i] != null; i++)
            {
                int g = i - top;
                GUI.SetNextControlName(player.items[i].name);
                GUI.Button(new Rect(Screen.width / 2 + 20, Screen.height / 2 + 80 + (20 * g) + (3 * g), 100, 20), player.items[i].name);
            }
            GUI.FocusControl(player.items[selectedItem].name);
        }
        if (isPerson)
        {
            GUI.Box(new Rect((Screen.width / 2) - 30, (Screen.height / 2) + 30, 200, 125), "Select an action:");
            if (movingPerson != null)
                GUI.Label(new Rect(Screen.width / 2 + 55, Screen.height / 2 + 50, 100, 20), "Select");
            for (int i = top; i <= bottom && player.persons[i] != null; i++)
            {
                int g = i - top;
                GUI.SetNextControlName(player.persons[i].forename);
                GUI.Button(new Rect(Screen.width / 2 + 20, Screen.height / 2 + 80 + (20 * g) + (3 * g), 100, 20), player.persons[i].forename);
            }
            GUI.FocusControl(player.persons[selectedItem].forename);
        }
    }
    
    void D(object a)
    {
        Debug.Log(a);
    }

    void HandleSelection()
    {
        switch (selectedItem)
        {
            case 0:
                isAttack = true;
                isReady = false;
                realSize = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (enemies[i] != null)
                    {
                        realSize++;
                    }
                }
                selectedItem = 0;
                top = 0;
                if (realSize < 3) bottom = realSize - 1;
                else bottom = 2;
                break;
            case 1:
                isMagic = true;
                isReady = false;
                selectedItem = 0;
                realSize = 0;
                for (int i = 0; i < movingPerson.magic.Length; i++)
                {
                    if (movingPerson.magic[i] != null)
                    {
                        realSize++;
                    }
                }
                if (realSize == 0)
                {
                    isMagic = false;
                    isReady = true;
                    selectedItem = 1;
                    return;
                }
                if (realSize < 3) bottom = realSize - 1;
                else bottom = 2;
                break;
            case 2:
                isItem = true;
                isReady = false;
                selectedItem = 0;
                realSize = 0;
                Array.Sort(player.items);
                Array.Reverse(player.items);
                for (int i = 0; i < player.items.Length; i++)
                {
                    if (player.items[i] != null)
                    {
                        realSize++;
                    }
                }
                if (realSize == 0)
                {
                    isItem = false;
                    isReady = true;
                    selectedItem = 2;
                    return;
                }
                if (realSize < 3) bottom = realSize - 1;
                else bottom = 2;
                break;
            case 3:
                Win();
                break;
            case 4:
                selectedItem = 0;
                realSize = 0;
                isReady = false;
                for (int i = 0; i < player.persons.Length; i++)
                {
                    if (player.persons[i] != null)
                    {
                        realSize++;
                    }
                }
                if (realSize < 3) bottom = realSize - 1;
                else bottom = 2;
                break;
        }
        isReady = false;
    }

    int MenuSelection(string direction)
    {
        int last = (isReady) ? 3 : realSize - 1;
        if (direction == "up")
        {
            if (selectedItem == 0)
            {
                selectedItem = last;
                bottom = last;
                if (last > 1) top = last - 2;
                else top = 0;
            }
            else
            {
                selectedItem -= 1;
                if (selectedItem == top - 1)
                {
                    top--;
                    bottom--;
                }
            }
        }

        if (direction == "down")
        {
            if (selectedItem == last)
            {
                selectedItem = 0;
                top = 0;
                if (last < 2) bottom = last;
                else bottom = 2;
            }
            else
            {
                selectedItem += 1;
                if (selectedItem == bottom + 1)
                {
                    top++;
                    bottom++;
                }
            }
        }

        return selectedItem;
    }

    void LoadGUI()
    {
        menuItems[0] = "Attack";
        menuItems[1] = "Magic";
        menuItems[2] = "Items";
        menuItems[3] = "Run";
    }

    void ShowPlayerPersons(Player player)
    {
        Array.Sort(player.persons);
        Array.Reverse(player.persons);
        for (int i = 0; i < 4; i++)
        {
            if (player.persons[i] != null)
            {
                string prefab = player.persons[i].prefab;
                GameObject person = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/" + prefab + ".prefab");
                player.persons[i].gameObject = Instantiate(person, new Vector3(8f + ((i == 1 || i == 2) ? 2 : 0), (float)(1 - (i * 1.35)), 0f), Quaternion.identity);
            }
        }
    }

    void ShowEnemies(Enemy[] enemies)
    {
        Array.Sort(enemies);
        Array.Reverse(enemies);
        for (int i = 0; i < 5; i++)
        {
            if (enemies[i] != null)
            {
                string prefab = enemies[i].prefab;
                GameObject person = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Enemies/" + prefab + ".prefab");
                enemies[i].gameObject = Instantiate(person, new Vector3(-10f + ((i == 1 || i == 3) ? 2 : 0), (float)(2.45 - (i * 1.55)), 0f), Quaternion.identity);
            }
        }
    }

    void BattleTiming()
    {
        if (!isReady && !isAttack && !isMagic && !isItem && !stop)
        {
            for (int i = 0; i < 4; i++)
            {
                if (player.persons[i] != null && player.persons[i].actionTime < 0)
                {
                    isReady = true;
                    movingPerson = player.persons[i];
                    player.persons[i].actionTime = player.persons[i].defaultActionTime - player.persons[i].speed / 1000;
                    return;
                }
                else if (player.persons[i] != null && !player.persons[i].isDead)
                {
                    player.persons[i].actionTime -= Time.deltaTime;
                }
            }
            for (int i = 0; i < 5; i++)
            {
                if (enemies[i] != null && enemies[i].actionTime < 0)
                {
                    enemies[i].actionTime = enemies[i].defaultActionTime;
                    enemies[i].StartAnimation();
                    EnemyAnswer(i);
                    return;
                }
                else if (enemies[i] != null)
                {
                    enemies[i].actionTime -= Time.deltaTime;
                }
            }
        }
    }

    void KeyboardControl()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            selectedItem = MenuSelection("down");
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            selectedItem = MenuSelection("up");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isReady)
            {
                HandleSelection();
                return;
            }
            if (isPerson && isItem)
            {
                player.items[cast].Action(player.persons[selectedItem]);
                isPerson = false;
                isItem = false;
                D("ok");
                top = 0;
                bottom = 2;
                selectedItem = 0;
                return;
            }
            if (isItem && !isAttack)
            {
                isPerson = true;
                cast = selectedItem;
                selectedItem = 4;
                HandleSelection();
                D(isPerson);
                D(isItem);
                return;
            }
            if ((isMagic || isItem) && !isAttack)
            {
                if (isItem == true && player.items[selectedItem].type != Item.Type.CONSUMABLE)
                {
                    return;
                }
                cast = selectedItem;
                isAttack = true;
                selectedItem = 0;
                HandleSelection();
                return;
            }
            if (isAttack)
            {
                Attack(selectedItem);
                return;
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isReady && (isMagic || isAttack || isItem))
            {
                CallReady();
            }
        }
    }

    void CallReady()
    {
        top = 0;
        bottom = 2;
        selectedItem = 0;
        isReady = true;
        isAttack = isMagic = isItem = false;
    }

    public IEnumerator returnPerson(float time, GameObject go)
    {
        yield return new WaitForSeconds(time);
        go.transform.position = new Vector3(x, y, 0);
        go.gameObject.GetComponent<Animator>().Play("Idle");
        stop = false;
    }

    void Attack(int enemy)
    {
        stop = true;
        
        x = movingPerson.gameObject.transform.position.x;
        y = movingPerson.gameObject.transform.position.y;

        movingPerson.gameObject.transform.position = new Vector3(enemies[enemy].gameObject.transform.position.x + 5, enemies[enemy].gameObject.transform.position.y, 0);

        StartCoroutine(returnPerson(0.5f, movingPerson.gameObject));

        if (isAttack && !isMagic && !isItem)
        {
            movingPerson.gameObject.GetComponent<Animator>().Play("Attack");
            enemies[enemy].hp -= movingPerson.weapon.damage;
        }

        if (isAttack && isItem)
        {
            D(enemies[enemy].hp);
            player.items[cast].Action(enemies[enemy]);
            if (player.items[cast].stack == 0)
            {
                player.items[cast] = null;
            }
            D(enemies[enemy].hp);
            isItem = false;
        }

        if (isAttack && isMagic)
        {
            StartCoroutine(movingPerson.magic[cast].timerDestroy(movingPerson.magic[cast].PlayAnimaion(movingPerson.gameObject), movingPerson.magic[cast].time));
            enemies[enemy].hp -= movingPerson.magic[cast].damage;
            isMagic = false;
        }
        
        if (enemies[enemy].hp <= 0)
        {
            Debug.Log(enemies[enemy].prefab + " killed");
            DestroyImmediate(enemies[enemy].gameObject, true);
            enemies[enemy] = null;
            Array.Sort(enemies);
            Array.Reverse(enemies);
            if (enemies[0] == null) Win();
        }
        selectedItem = 0;
        top = 0;
        bottom = 2;
        isAttack = false;
    }

    void EnemyAnswer(int enemy)
    {
        bool z = true;
        for (int i = 0; i < 4; i++)
        {
            if (player.persons[i] != null && !player.persons[i].isDead)
            {
                z = false;
            }
        }
        if (z) return;
        stop = true;

        this.x = (int) enemies[enemy].gameObject.transform.position.x;
        this.y = (int) enemies[enemy].gameObject.transform.position.y;

        int x = UnityEngine.Random.Range(0, 3);
        while (player.persons[x] == null || player.persons[x].isDead)
        {
            x = UnityEngine.Random.Range(0, 3);
        }

        Debug.Log(x);
        enemies[enemy].gameObject.transform.position = new Vector3(player.persons[x].gameObject.transform.position.x - 5, player.persons[x].gameObject.transform.position.y, 0);

        StartCoroutine(returnPerson(0.5f, enemies[enemy].gameObject));
        player.persons[x].currentHealth -= enemies[enemy].GetRealDamage();
        if (player.persons[x].currentHealth < 0)
        {
            player.persons[x].isDead = true;
            player.persons[x].actionTime = 3f;
            player.persons[x].gameObject.SetActive(false);
        }
    }

    void Win()
    {
        PlayerData playerData = GameObject.Find("PlayerData").GetComponent<PlayerData>();
        string location = playerData.location;
        playerData.location = "Fight";
        GameObject dialogBulider = GameObject.Find("DialogBulider");
        dialogBulider = new GameObject("DialogBulider");
        DontDestroyOnLoad(dialogBulider);
        dialogBulider.AddComponent<DialogGUI>();
        Debug.Log(location);
        SceneManager.LoadScene(location);
    }
}
