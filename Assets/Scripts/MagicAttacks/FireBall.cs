﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class FireBall : Magic {

    GameObject fireBall;

    public FireBall()
    {
        time = 0.3f;
        damage = 20;
        type = Type.FIRE;
        forename = "Fireball";
    }

    public override GameObject PlayAnimaion(GameObject person)
    {
        fireBall = GameObject.Instantiate(UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Magic/Fireball/Fireball.prefab"), new Vector3(person.transform.position.x - 1, person.transform.position.y), Quaternion.identity);
        fireBall.AddComponent<LeftDirMove>();
        return fireBall;
    }

    public override IEnumerator timerDestroy(GameObject destroy, float time)
    {
        yield return new WaitForSeconds(time);
        GameObject.Destroy(destroy);
    }
}
