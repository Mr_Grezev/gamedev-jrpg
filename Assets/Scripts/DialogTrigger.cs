﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogTrigger : MonoBehaviour
{

    public string dialogTag;
    PlayerData playerData;
    DialogGUI dialog;

    void OnTriggerEnter2D(Collider2D col)
    {
        PlayerData playerData = GameObject.Find("PlayerData").GetComponent<PlayerData>();
        DialogGUI dialog = GameObject.Find("DialogBulider").GetComponent<DialogGUI>();
        if (!playerData.points.ContainsKey(dialogTag))
        {
            playerData.points.Add(dialogTag, true);
        }
        if (playerData.points.ContainsKey(dialogTag) && playerData.points[dialogTag])
        {
            Debug.Log("ok");
            //Тут писать действия, которые произойдут при входе.
            dialog.AddDialogMesseage("", "");
            //
            playerData.points[dialogTag] = false;
        }
    }
}
