﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public Vector2 speed = new Vector2(1,1);
    private Vector2 movement;
    private Rigidbody2D rb;
    private Animator anim;
    


    void Start(){
        
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
	
	void Update () {

        float moveX = Input.GetAxis("Horizontal");
        float moveY = Input.GetAxis("Vertical");

        movement = new Vector2(speed.x * moveX, speed.y * moveY);
        rb.velocity = movement;

        anim.SetFloat("Speed", Mathf.Abs(moveX));

        GameObject playerData = GameObject.Find("PlayerData");

        if ((moveX !=0 || moveY != 0) && playerData != null && playerData.GetComponent<PlayerData>().fightLocation)
        {
            int r = (int) Random.Range(0f,3500f);
            if(r < 3)
            {
                (new FightBuffer()).Fight(false, FightBuffer.MOB_PACK_1);
            }
        }
        
        if (moveX > 0) {
            anim.CrossFade("AlexRight", Time.deltaTime);
        }
            

        else if (moveX <0) {
            anim.CrossFade("AlexLeft", Time.deltaTime);
        }
            

        else if (moveY < 0) {
            anim.CrossFade("AlexDown", Time.deltaTime);
        }

        else if (moveY > 0)
        {
            anim.CrossFade("AlexUp", Time.deltaTime);
        }

        if (moveX == 0 && moveY == 0) {
            anim.CrossFade("AlexStay", Time.deltaTime);
        }

    }

    

  
}
