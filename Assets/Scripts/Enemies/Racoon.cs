﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racoon : Enemy {
    public Racoon()
    {
        hp =20;
        mp = 0;
        lv = 1;
        damage = 3;
        prefab = "Racoon";
        speed = 5;
        weakness = Magic.Type.NONE;
        SetDefaults();
    }
	
}
