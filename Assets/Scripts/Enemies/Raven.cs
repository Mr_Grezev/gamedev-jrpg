﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Raven : Enemy {
    public Raven()
    {
        hp = 15;
        mp = 0;
        lv = 3;
        damage = 3;
        prefab = "Raven";
        speed = 5;
        weakness = Magic.Type.FIRE;
        SetDefaults();
    }
}
