﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HellDog : Enemy {
    public HellDog()
    {
        hp = 10;
        mp = 0;
        lv = 1;
        damage = 2;
        prefab = "HellDog";
        speed = 5;
        weakness = Magic.Type.NONE;
        SetDefaults();
    }
	
}
