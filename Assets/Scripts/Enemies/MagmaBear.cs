﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagmaBear : Enemy {
    public MagmaBear()
    {
        hp = 500;
        mp = 0;
        lv = 3;
        damage = 2;
        prefab = "MagmaBear";
        speed = 5;
        weakness = Magic.Type.NONE;
        SetDefaults();
    }
	
}
