﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiniMenu : MonoBehaviour {

    SaveAndLoad saver = new SaveAndLoad();
   
    public GameObject player;

    const int MENU_SIZE = 2;

    bool open = false;
    string[] menu = new string[2];
    int selectedItem = 0;
    float time;

    public void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        menu[0] = "Сохранить";
        menu[1] = "Главное меню";
    }

    void HandleSelection()
    {
        switch (selectedItem)
        {
            case 0:
                saver.Save(player, GameObject.Find("PlayerData").GetComponent<PlayerData>(), true);
                break;
            case 1:
                Time.timeScale = time;
                Destroy(GameObject.Find("PlayerData"));
                SceneManager.LoadScene("Menu");
                break;
        }
    }

    void OnGUI()
    {
        if (open == true)
        {
            
            GUI.Box(new Rect((Screen.width / 2) - 100, (Screen.height / 2) - 150, 200, 150), "Мини меню");
            GUI.SetNextControlName(menu[0]);
            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 100, 150, 25), "Сохранить"))
            {
                
                
            }
            GUI.SetNextControlName(menu[1]);
            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 50, 150, 25), "В главное меню"))
            {
                
            }

            GUI.FocusControl(menu[selectedItem]);
        }
    }

    void KeyboardControl()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            selectedItem = MenuSelection("down");
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            selectedItem = MenuSelection("up");
        }

        if (Input.GetKeyDown(KeyCode.Space) && open)
        {
            HandleSelection();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (open == true)
            {
                player.GetComponent<PlayerControl>().speed = new Vector2(100, 100);
                Time.timeScale = time;
                open = false;
                selectedItem = 0;
            }
            else
            {
                player.GetComponent<PlayerControl>().speed = new Vector2(0, 0);
                time = Time.timeScale;
                Time.timeScale = 0;
                open = true;
            }
        }
    }

    int MenuSelection(string direction)
    {
        if (direction == "up")
        {
            if (selectedItem == 0)
            {
                selectedItem = MENU_SIZE - 1;
            }
            else
            {
                selectedItem -= 1;
            }
        }

        if (direction == "down")
        {
            if (selectedItem == MENU_SIZE - 1)
            {
                selectedItem = 0;
            }
            else
            {
                selectedItem += 1;
            }
        }

        return selectedItem;
    }

    void Update()
    {
        KeyboardControl();
    }
    
}
