﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemLoadScript : MonoBehaviour
{

    public GameObject player;

    void Start()
    {
        GameObject playerData = GameObject.Find("PlayerData");
        GameObject dialogBulider = GameObject.Find("DialogBulider");
        DialogGUI dialog = dialogBulider.GetComponent<DialogGUI>();
        string location = playerData.GetComponent<PlayerData>().location;
        Debug.Log("1");
        playerData.GetComponent<PlayerData>().fightLocation = false;
        switch (location)
        {
            case "Game":
                player.transform.position = new Vector3(3730, -95);
                break;
            case "Home":
                player.transform.position = new Vector3(6100, -1440);
                break;
            case "City":
                player.transform.position = new Vector3(2850, -1775);
                break;
            default:
                player.transform.position = new Vector3(5850.879f, -3114.692f);
                break;
        }
        playerData.GetComponent<PlayerData>().location = "Gem";
        SaveAndLoad loader = new SaveAndLoad();
        loader.Load(player, playerData.GetComponent<PlayerData>());
    }

    void Update()
    {

    }
}
