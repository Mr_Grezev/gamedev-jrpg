﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeLoadScript : MonoBehaviour
{

    public GameObject player;

    void Start()
    {
        GameObject playerData = GameObject.Find("PlayerData");
        string location = playerData.GetComponent<PlayerData>().location;
        playerData.GetComponent<PlayerData>().fightLocation = false;
        Debug.Log("2");
        switch (location)
        {
            case "Gem":
                Debug.Log("3");
                player.transform.position = new Vector3(608, -900); 
                break;
            default:
                player.transform.position = new Vector3(playerData.GetComponent<PlayerData>().x, playerData.GetComponent<PlayerData>().y);
                break;
        }
        playerData.GetComponent<PlayerData>().location = "Home";
        SaveAndLoad loader = new SaveAndLoad();
        loader.Load(player, playerData.GetComponent<PlayerData>());
    }

    void Update()
    {

    }
}