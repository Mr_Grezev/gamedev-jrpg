﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChinaLoadScript : MonoBehaviour {

    public GameObject player;
 
    void Start () {
        GameObject playerData = GameObject.Find("PlayerData");
        string location = playerData.GetComponent<PlayerData>().location;
        playerData.GetComponent<PlayerData>().location = "China";
        switch (location)
        {
            case "Game":
                player.transform.position = new Vector3(1000,-1000);
                break;
            default:
                player.transform.position = new Vector3(playerData.GetComponent<PlayerData>().x, playerData.GetComponent<PlayerData>().y);
                break;
        }

        SaveAndLoad loader = new SaveAndLoad();
        loader.Load(player, playerData.GetComponent<PlayerData>());
    }
	
	void Update () {
		
	}
}
