﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityLoadScript : MonoBehaviour
{

    public GameObject player;

    void Start()
    {
        GameObject playerData = GameObject.Find("PlayerData");
        string location = playerData.GetComponent<PlayerData>().location;
        playerData.GetComponent<PlayerData>().fightLocation = true;
        switch (location)
        {
            case "Gem":
                player.transform.position = new Vector3(2945, -1775);
                break;
            default:
                player.transform.position = new Vector3(playerData.GetComponent<PlayerData>().x, playerData.GetComponent<PlayerData>().y);
                break;
        }
        playerData.GetComponent<PlayerData>().location = "City";
        SaveAndLoad loader = new SaveAndLoad();
        loader.Load(player, playerData.GetComponent<PlayerData>());
    }

    void Update()
    {

    }
}