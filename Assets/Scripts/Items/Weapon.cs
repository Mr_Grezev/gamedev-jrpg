﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

abstract public class Weapon : Item {
    public int damage;

    public override void Action(Being being)
    {
        throw new System.NotImplementedException();
    }

    public Weapon()
    {
        stack = 1;
    }
}
