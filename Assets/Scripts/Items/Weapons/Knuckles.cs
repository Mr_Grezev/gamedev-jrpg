﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Knuckles : Weapon {
    public Knuckles()
    {
        damage = 200;
    }
}
