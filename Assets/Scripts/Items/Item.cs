﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public abstract class Item : System.IComparable<Item> {
    public string name;
    public int type;
    public int stack;

    abstract public void Action(Being being);

    public int CompareTo(Item other)
    {
        return name.CompareTo(other.name);
    }

    public static class Type {
        public const int CONSUMABLE = 0;
        public const int HEALING = 1;
        public const int ARMOR = 2;
        public const int WEAPON = 3;
    }
}
