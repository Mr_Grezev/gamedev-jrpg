﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class HealthPotion : Item {
    public HealthPotion()
    {
        name = "Health Potion";
        type = Type.HEALING;
        stack = 60;
    }

    public override void Action(Being being)
    {
        if (stack != 0)
        {
            being.currentHealth += 5;
            if (being.currentHealth < being.hp) being.currentHealth = being.hp;
            stack--;
        }
    }
}
