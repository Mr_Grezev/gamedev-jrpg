﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Elena : Person
{
    public Elena()
    {
        SetParams("Elena", "Elena/Elena", new Knuckles(), 200, 15, 1, 5);
    }

}
