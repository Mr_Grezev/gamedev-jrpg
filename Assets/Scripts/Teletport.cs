﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletport : MonoBehaviour {

    public GameObject player;
    public float x;
    public float y;
    void OnTriggerEnter2D(Collider2D col)
    {

        player.gameObject.transform.position = new Vector3(x, y);

    }
}

