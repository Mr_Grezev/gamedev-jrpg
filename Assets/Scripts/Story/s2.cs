﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s2 : MonoBehaviour {

    public string dialogTag;
    PlayerData playerData;
    DialogGUI dialog;
    
    void OnTriggerEnter2D(Collider2D col)
    {
        PlayerData playerData = GameObject.Find("PlayerData").GetComponent<PlayerData>();
        DialogGUI dialog = GameObject.Find("DialogBulider").GetComponent<DialogGUI>();
        if (!playerData.points.ContainsKey(dialogTag))
        {
            playerData.points.Add(dialogTag, true);
        }
        if (playerData.points.ContainsKey(dialogTag) && playerData.points[dialogTag])
        {
            Debug.Log("ok");
            dialog.AddDialogMesseage("Чей-то голос1", "МОНСТРЫ НАПАДАЮТ!!! БЕГИТЕ, КТО МОЖЕТ!!!");
            dialog.AddDialogMesseage("Чей-то голос2", "Снова монстры прорываются к нам. Уже отдали им добрую часть города. \n А они все лезут. Интересно, отобьются ли герои?");
            dialog.AddDialogMesseage("Чей-то голос3", "Да отобьются. Эти ворота и стена, что находятся на западе, отобьют любую их атаку. \n Жалко, что героев становится меньше.");
            dialog.AddDialogMesseage("Алекс", "(шепотом) Надо зайти домой. Без меча отца, я буду бессилен против монстров.");
            // ");dialog.AddDialogMesseage("Алекс", "
            playerData.points[dialogTag] = false;
        }
    }
}
