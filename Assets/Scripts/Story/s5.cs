﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s5 : MonoBehaviour {

    public string dialogTag;
    PlayerData playerData;
    DialogGUI dialog;
    
    void OnTriggerEnter2D(Collider2D col)
    {
        PlayerData playerData = GameObject.Find("PlayerData").GetComponent<PlayerData>();
        DialogGUI dialog = GameObject.Find("DialogBulider").GetComponent<DialogGUI>();
        if (!playerData.points.ContainsKey(dialogTag))
        {
            playerData.points.Add(dialogTag, true);
        }
        if (playerData.points.ContainsKey(dialogTag) && playerData.points[dialogTag])
        {
            Debug.Log("ok");
            dialog.AddDialogMesseage("Алекс", "Отлично! Мне нужно еще поохотиться на монстров.");
            dialog.AddDialogMesseage("Алекс", "Благодарим за игру в демо версию данной игры.");
            dialog.AddDialogMesseage("Доступна новая способность", "FireBall");
            playerData.player.persons[0].magic[0] = new FireBall();
            // ");dialog.AddDialogMesseage("Алекс", "
            playerData.points[dialogTag] = false;
        }
    }
}
