﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s4 : MonoBehaviour {

    public string dialogTag;
    PlayerData playerData;
    DialogGUI dialog;
    
    void OnTriggerEnter2D(Collider2D col)
    {
        PlayerData playerData = GameObject.Find("PlayerData").GetComponent<PlayerData>();
        DialogGUI dialog = GameObject.Find("DialogBulider").GetComponent<DialogGUI>();
        if (!playerData.points.ContainsKey(dialogTag))
        {
            playerData.points.Add(dialogTag, true);
        }
        if (playerData.points.ContainsKey(dialogTag) && playerData.points[dialogTag])
        {
            Debug.Log("ok");
            (new FightBuffer()).Fight(false, FightBuffer.MOB_PACK_1);
            // ");dialog.AddDialogMesseage("Алекс", "
            playerData.points[dialogTag] = false;
        }
    }
}
