﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class s3 : MonoBehaviour {

    public string dialogTag;
    PlayerData playerData;
    DialogGUI dialog;
    
    void OnTriggerEnter2D(Collider2D col)
    {
        PlayerData playerData = GameObject.Find("PlayerData").GetComponent<PlayerData>();
        DialogGUI dialog = GameObject.Find("DialogBulider").GetComponent<DialogGUI>();
        if (!playerData.points.ContainsKey(dialogTag))
        {
            playerData.points.Add(dialogTag, true);
        }
        if (playerData.points.ContainsKey(dialogTag) && playerData.points[dialogTag])
        {
            Debug.Log("ok");
            dialog.AddDialogMesseage("Алекс", "Клинок отца...\nОн часто мне рассказывал, что это его первый боевой клинок.\nОн весь покрыл царапинами и сколами, но до сих пор содержит ту его частичку,\nгде Отец каждый день точил и полировал его.");
            // ");dialog.AddDialogMesseage("Алекс", "
            playerData.points[dialogTag] = false;
        }
    }
}
