﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Player {

    public Person[] persons = new Person[4];
    public Item[] items = new Item[1000];
    public int money;

    public Player()
    {
        
    }
}
