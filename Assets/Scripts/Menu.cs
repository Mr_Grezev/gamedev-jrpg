﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Menu : MonoBehaviour {


    public GameObject buttonsMenu;
    public GameObject exitButtons;

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

	public void NewGamePressed () {
        GameObject playerData = GameObject.Find("PlayerData");
        if (!playerData)
        {
            Debug.Log("Не ок");
            playerData = new GameObject("PlayerData");
            DontDestroyOnLoad(playerData);
            playerData.AddComponent<PlayerData>().player = new Player();
            playerData.GetComponent<PlayerData>().player.persons[0] = new Alex();
            playerData.GetComponent<PlayerData>().player.money = 100;
            playerData.GetComponent<PlayerData>().session = Random.Range(0, 100000);
        }
        GameObject dialogBulider = GameObject.Find("DialogBulider");
        if (!dialogBulider)
        {
            Debug.Log("Диалог создан");
            dialogBulider = new GameObject("DialogBulider");
            DontDestroyOnLoad(dialogBulider);
            dialogBulider.AddComponent<DialogGUI>();
        }
        loader.Save(new GameObject(), new PlayerData(), false);
        SceneManager.LoadScene("Gem");
	}

    public void ShowExitButtons(){

        buttonsMenu.SetActive(false);
        exitButtons.SetActive(true);
    }

    public void BackInMenu()
    {

        buttonsMenu.SetActive(true);
        exitButtons.SetActive(false);
    }

    SaveAndLoad loader = new SaveAndLoad();

    public void Load()
    {
        GameObject playerData = GameObject.Find("PlayerData");
        if (!playerData)
        {
            Debug.Log("Не ок");
            playerData = new GameObject("PlayerData");
            DontDestroyOnLoad(playerData);
            playerData.AddComponent<PlayerData>().player = new Player();
        }
        GameObject dialogBulider = GameObject.Find("DialogBulider");
        if (!dialogBulider)
        {
            Debug.Log("Диалог создан");
            dialogBulider = new GameObject("DialogBulider");
            DontDestroyOnLoad(dialogBulider);
            dialogBulider.AddComponent<DialogGUI>();
        }
        string scene = "";
        scene = loader.LoadScene();
        Debug.Log(scene);
        SceneManager.LoadScene(scene);
    }

    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("Exit pressed!");
    }

   

}

